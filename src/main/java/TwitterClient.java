import java.io.File;
import java.nio.file.Files;

import javax.ws.rs.core.MediaType;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;

public class TwitterClient {

	public static void main(String[] args) {

	    try {

	        Client client = Client.create();

	        WebResource webResource = client.resource("http://localhost:8080/TwitterService/webresources/myresource/post");
		    String json="[  \r\n" + 
			"   {  \r\n" + 
			"      \"id\":\"1\",\r\n" + 
			"      \"labels\":[  \r\n" + 
			"         {  \r\n" + 
			"            \"text\":\"rain\",\r\n" + 
			"            \"score\":0.320496732575757\r\n" + 
			"         }\r\n" + 
			"        ],\r\n" + 
			"\"location\":{  \r\n" + 
			"         \"latitude\":45.5455,\r\n" + 
			"         \"longitude\":11.5354\r\n" + 
			"      },\r\n" + 
			"      \"tweets\":[\r\n" + 
			"        \"1001376594013765632\"\r\n" + 
			"      ],\r\n" + 
			"      \"top_ranked_tweets\":[  \r\n" + 
			"         \"1001376594013765632\"\r\n" + 
			"      ]\r\n" + 
			"   }\r\n" + 
			"]\r\n" + 
			"";


	        ClientResponse response = webResource.type(MediaType.APPLICATION_JSON).post(ClientResponse.class, json);
	        if (response.getStatus() != 201) {
	            throw new RuntimeException("Failed : HTTP error code : " + response.getStatus());
	        }

	        System.out.println("Output from Server .... \n");
	        // The results after convertion
	        String output = response.getEntity(String.class);
	        System.out.println(output);
	        
	      } catch (Exception e) {

	        e.printStackTrace();

	      }
	}

}
